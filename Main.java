
public class Main implements InSample1,InSample2 {
	public void print() {
		System.out.println(InSample2.C_NAME);
	}
	public void print(String text) {
		System.out.println(InSample1.S_NAME + text);
	}
	public static void main(String[] args) {
		InSample1 in1 = new Main();
		InSample2 in2 = new Main();
		in1.print("システム作成");
		in2.print();
	}

}
